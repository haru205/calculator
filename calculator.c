#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <limits.h>
#include "calculator.h"

int contest_process(Calculator *this){
    reset_calculator(this);
    get_line(this);
    find_all_variables(this);
    if (this->err != NOTHING) {
        print_error_msg(this); return 1;
    }
    sort_var_store(this->var_store);
    calculating(this, CHECK);
    if (this->err != NOTHING) {
        print_error_msg(this); return 1;
    }
    clean_sign_stack(this->sign_stack);
    clean_val_stack(this->val_stack);
    input_all_variables(this);
    if (this->err != NOTHING) {
        print_error_msg(this); return 2;
    }
    calculating(this, DEFAULT);
    if (this->err != NOTHING) {
        print_error_msg(this); return 3;
    }
#ifdef int_ari
    printf("%ld\n", this->val_stack->mas[0]);
#endif
#ifdef float_ari
    printf("%g\n", this->val_stack->mas[0]);
#endif
    return 0;
}

void user_process(Calculator *this){
    while (1){
        reset_calculator(this);
        printf(">>>");
        get_line(this);
        if (check_start(this, "$exit")){
            return;
        }
        calculating(this, CHECK);
        if (this->err != NOTHING){
            print_error_msg(this);
            continue;
        }
        clean_sign_stack(this->sign_stack);
        clean_val_stack(this->val_stack);
        calculating(this, DEFAULT);
        if (this->err != NOTHING){
            print_error_msg(this);
            continue;
        }

#ifdef int_ari
        printf("%ld\n", this->val_stack->mas[0]);
#endif
#ifdef float_ari
        printf("%g\n", this->val_stack->mas[0]);
#endif
    }
}

int calculating(Calculator *this, CalculatingMode mode){
    this->ptr = 0;
    if (mode != CHECK && mode != DEFAULT){
        this->err = UNKNOWN_MODE;
        return UNKNOWN_MODE;
    }
    int code;
    // cnt counting number of values before input another sign
    int cnt = 0;
    while (1){
        get_tok(this);
        if (this->type == END || this->type == ERROR){
            break;
        }else if (this->type == VALUE){
            cnt++;
            // get_tok() guarantees correctness
            if (mode == DEFAULT){
#ifdef int_ari
                push_value(this->val_stack, strtol(this->token, NULL, 10));
#endif
#ifdef float_ari
                push_value(this->val_stack, strtod(this->token, NULL));
#endif
            }else{
                push_value(this->val_stack, 0);
            }
        }else if (this->type == VARIABLE){
            cnt++;
            if (mode == DEFAULT){
                Variable *check = get_var(this->var_store, this->token);
                if (check == NULL){
                    process_variable(this);
                }else{
                    push_value(this->val_stack, check->value);
                }
            }else{
                push_value(this->val_stack, 0);
            }
        }else{
            if (0 != (code = process_sign(this, cnt, mode))){
                this->err = code;
                return code;
            }
            if (this->type == PLUS || this->type == MINUS ||
                    this->type == MULTIPLY || this->type == DIVIDE){
                cnt = 0;
            }
        }
    }

    if (this->err == NOTHING){
        while (this->sign_stack->size != 0){
            if (0 != (code = calculate_top(this, mode))){
                this->err = code;
                return code;
            }
        }
    }

    if (this->err == NOTHING && this->val_stack->size >= 2){
        this->err = MISS_OPERATOR;
        return MISS_OPERATOR;
    }

    if (this->err == NOTHING && this->val_stack->size == 0){
        this->err = EMPTY_LINE;
        return EMPTY_LINE;
    }
    this->ptr = 0;
    return 0;
}

int process_sign(Calculator *this, int cnt, CalculatingMode mode){
    int code;
    if (cnt == 0){
        if (this->type == PLUS){
            this->type = UN_PLUS;
            this->token[0] = MAC_UN_PLUS;
        }else if (this->type == MINUS){
            this->type = UN_MINUS;
            this->token[0] = MAC_UN_MINUS;
        }else if (this->type != OPEN && this->type != CLOSE){
            return MISS_OPERAND;
        }
    }

    int lvl_up = get_level(this->token[0]);
    while (1){
        int status;
        char down = top_sign(this->sign_stack, &status);
        if (status != 0) down = EOF;
        int lvl_down = get_level(down);
        if (this->token[0] == '(' || lvl_down < lvl_up){
            push_sign(this->sign_stack, this->token[0]);
            return 0;
        }else if (lvl_down > lvl_up){
            if (0 != (code = calculate_top(this, mode))){
                return code;
            }
            continue;
        }else{
            if (this->token[0] == ')'){
                if (down == EOF){
                    return MISS_OPEN_BRACKET;
                }
                pop_sign(this->sign_stack);
                return 0;
            }
            if (lvl_up == 4){
                push_sign(this->sign_stack, this->token[0]);
                return 0;
            }
            if (0 != (code = calculate_top(this, mode))){
                return code;
            }
            continue;
        }
    }
}

int get_level(int sym){
    switch (sym){
        case ')':
        case '(':
        case EOF:
            return 1;
        case '+':
        case '-':
            return 2;
        case '*':
         case '/':
            return 3;
        case MAC_UN_MINUS:
        case MAC_UN_PLUS:
            return 4;
        default:
            return UNKNOWN_OPERATOR;
    }
}

int calculate_top(Calculator *this, CalculatingMode mode){
    if (mode != DEFAULT && mode != CHECK){
        this->err = UNKNOWN_MODE;
        return UNKNOWN_MODE;
    }
    int status;
    char sign = top_sign(this->sign_stack, &status);
    if (status != 0){
        return status;
    }
    pop_sign(this->sign_stack);
    if (sign == '+' || sign == '-' || sign == '*' || sign == '/'){
#ifdef int_ari
        long long res;
#else
        val_type res;
#endif
        val_type a, b;
        b = top_value(this->val_stack, &status);
        if (status != 0){
            return status;
        }
        pop_value(this->val_stack);
        a = top_value(this->val_stack, &status);
        if (status != 0){
            return status;
        }
        pop_value(this->val_stack);
        if (mode == DEFAULT){
            switch (sign){
                case '+': { res = a + b; break; }
                case '-': { res = a - b; break; }
                case '*': { res = a * b; break; }
                case '/':
                {
#ifdef int_ari
                    if (b == 0) { return NULL_DIVIDING; }
#endif
#ifdef float_ari
                    if (float_compare(b, 0)) { return NULL_DIVIDING; }
#endif
                    res = a / b;
                    break;
                }
                default:
                    // impossible
                    return NOT_CALCULABLE_SIGN;
            }

#ifdef int_ari
            if (res > (long long) INT_MAX || res < (long long) INT_MIN){
                return OUT_RANGE;
            }
#endif
            push_value(this->val_stack, res);
        }else{
            push_value(this->val_stack, 0);
        }
        return 0;
    }else if (sign == MAC_UN_PLUS){
        return 0;
    }else if (sign == MAC_UN_MINUS){
        val_type b;
        if (mode == DEFAULT){
            b = top_value(this->val_stack, &status);
            if (status != 0){
                return status;
            }
            pop_value(this->val_stack);
            push_value(this->val_stack, -1 * b);
        }
        return 0;
    }else{
        return NOT_CALCULABLE_SIGN;
    }
}

Calculator* init_calculator(void){
    Calculator *this = malloc(sizeof (Calculator));
    this->err_file = stderr;
    this->str = NULL;
    this->val_stack = init_val_stack();
    this->sign_stack = init_sign_stack();
    this->var_store = init_var_store();
    reset_calculator(this);
    return this;
}

void reset_calculator(Calculator *this){
    this->err = NOTHING;
    free(this->str);
    this->service = 5;
    this->str_size = 0;
    this->str = malloc(sizeof (char) * this->service);
    this->str[0] = '\0';
    this->ptr = 0;
    this->tok_size = 0;
    this->token[this->tok_size] = '\0';
    this->type = END;
    clean_sign_stack(this->sign_stack);
    clean_val_stack(this->val_stack);
}

void delete_calculator(Calculator *this){
    if (this == NULL) return;
    delete_var_store(this->var_store);
    delete_val_stack(this->val_stack);
    delete_sign_stack(this->sign_stack);
    free(this->str);
    free(this);
}

void get_tok(Calculator *this){
    while (isspace(this->str[this->ptr]) &&
            this->str[this->ptr] != '\0' &&
                    this->str[this->ptr] != '\n'){
        this->ptr++;
    }
    if (check_service_sym(this->str[this->ptr])){
        switch (this->str[this->ptr]){
            case '+': { this->type = PLUS; break; }
            case '-': { this->type = MINUS; break; }
            case '*': { this->type = MULTIPLY; break; }
            case '/': { this->type = DIVIDE; break; }
            case '(': { this->type = OPEN; break; }
            case ')': { this->type = CLOSE; break; }
            case '\0':
            case '\n': { this->type = END; return; }
            default:
                break;
        }
        this->tok_size = 1;
        this->token[0] = this->str[this->ptr];
        this->token[1] = '\0';
        if (this->str[this->ptr] != '\0'){
            this->ptr++;
        }
        return;
    }

    long tok_end = find_service_sym(this->str, this->ptr);
    if (tok_end - this->ptr > MAX_TOK_LENGTH){
        this->type = ERROR;
        this->err = BIG_TOK_ERR;
        this->tok_size = 0;
        this->token[0] = '\0';
        return;
    }

    this->tok_size = tok_end - this->ptr;
    memcpy(this->token, this->str + this->ptr, this->tok_size);
    this->token[this->tok_size] = '\0';
    if (isdigit(this->str[this->ptr])){
        if (this->type == UN_MINUS){
            pop_sign(this->sign_stack);
            memmove(this->token + 1, this->token, this->tok_size + 1);
            this->token[0] = '-';
        }
        char *end;
#ifdef int_ari
        long long val = strtol(this->token, &end, 10);
#endif
#ifdef float_ari
        strtod(this->token, &end);
#endif
        if (errno == ERANGE || end - this->token != strlen(this->token)){
            errno = 0;
            this->type = ERROR;
            this->err = VALUE_TOK_ERR;
            return;
        }
#ifdef int_ari
        if (val < -2147483648ll || val > 2147483647ll){
            this->type = ERROR;
            this->err = OUT_RANGE;
            return;
        }
#endif
        this->type = VALUE;
    }else /* suppose it is variable */{
        if (this->token[0] != '_' && !isalpha(this->token[0])){
            this->type = ERROR;
            this->err = VARIABLE_TOK_ERR;
            return;
        }
        for (long i = 1; i < tok_end - this->ptr; i++){
            if (!isalpha(this->token[i]) &&
                    !isdigit(this->token[i]) &&
                            this->token[i] != '_'){
                this->type = ERROR;
                this->err = VARIABLE_TOK_ERR;
                return;
            }
        }

        if (tok_end - this->ptr > MAX_VARIABLE_LENGTH){
            this->type = ERROR;
            this->err = BIG_VARIABLE_ERR;
            return;
        }
        this->type = VARIABLE;
    }
    this->ptr = tok_end;
}

void get_line(Calculator *this){
    int sym;
    while (1){
        if (this->str_size + 1 >= this->service){
            this->service += 1000;
            this->str = realloc(this->str, this->service);
        }
        sym = getchar();
        if (sym == '\n' || sym == EOF){
            this->str[this->str_size] = '\0';
            break;
        }
        this->str[this->str_size++] = (char) sym;
    }
}

int check_start(Calculator *this, char *str){
    long n = 0;
    while (isspace(this->str[n]) &&
            this->str[n] != '\0' && this->str[n] != EOF){
        n++;
    }
    if (n == this->str_size){
        return 0;
    }
    if (strstr(this->str + n, str) == this->str + n){
        return 1;
    }else{
        return 0;
    }
}

long find_service_sym(char *dest, long start){
    while (!check_service_sym(dest[start])){
        start++;
    }
    return start;
}

int check_service_sym(int sym){
    if (isspace(sym)){
        return 1;
    }
    switch (sym){
        case '+':case '-':
        case '*':case '/':
        case '(':case ')':
        case '\0':
        case '\n':
            return 1;
        default:
            return 0;
    }
}

void print_error_msg(Calculator *this){
    switch (this->err){
        case OUT_RANGE:
            fprintf(this->err_file, "Out range\n");
            break;
        case EMPTY_LINE:
            fprintf(this->err_file, "Empty input\n");
            break;
        case NULL_DIVIDING:
            fprintf(this->err_file, "Division by zero\n");
            break;
        case MISS_OPERATOR:
        case EMPTY_SIGN_STACK:
            fprintf(this->err_file, "Missing operator\n");
            break;
        case EMPTY_VALUE_STACK:
        case MISS_OPERAND:
            fprintf(this->err_file, "Missing operand\n");
            break;
        case MISS_OPEN_BRACKET:
            fprintf(this->err_file, "Missing open bracket\n");
            break;
        case MISS_CLOSE_BRACKET:
            fprintf(this->err_file, "Missing close bracket\n");
            break;
        case VARIABLE_TOK_ERR:
            fprintf(this->err_file, "Wrong variable token\n");
            fprintf(this->err_file, "Token: %s\n", this->token);
            break;
        case VALUE_TOK_ERR:
            fprintf(this->err_file, "Wrong value token\n");
            fprintf(this->err_file, "Token: %s\n", this->token);
            break;
        case BIG_TOK_ERR:
            fprintf(this->err_file, "The token is too long\n");
            print_current_token(this);
            break;
        case BIG_VARIABLE_ERR:
            fprintf(this->err_file, "The variable is too long\n");
            fprintf(this->err_file, "Token: %s\n", this->token);
            break;
        case NOTHING:
            fprintf(this->err_file, "No errors occurred\n");
            break;
        default:
            fprintf(this->err_file, "Error is occurred\n");
            break;
    }
}

void print_current_token(Calculator *this){
    long end = find_service_sym(this->str, this->ptr);
    char rem = this->str[end];
    this->str[end] = '\0';
    printf("%s\n", this->str + this->ptr);
    this->str[end] = rem;
}

ValStack* init_val_stack(){
    ValStack *stack = malloc(sizeof(ValStack));
    stack->size = 0;
    stack->service = 5;
    stack->mas = malloc(sizeof(val_type) * stack->service);
    return stack;
}

void delete_val_stack(ValStack *stack){
    if (stack == NULL){
        return;
    }
    free(stack->mas);
    free(stack);
}

void push_value(ValStack *stack, val_type value){
    if (stack->size >= stack->service){
        stack->service += 50;
        stack->mas = realloc(stack->mas, sizeof (val_type) * stack->service);
    }
    stack->mas[stack->size++] = value;
}

int pop_value(ValStack *stack){
    if (stack->size <= 0){
        return EMPTY_VALUE_STACK;
    }
    stack->size--;
    return 0;
}=

val_type top_value(ValStack *stack, int *status){
    if (stack->size <= 0){
        if (status != NULL){
            *status = EMPTY_VALUE_STACK;
        }
        return 0;
    }
    if (status != NULL){
        *status = 0;
    }
    return stack->mas[stack->size - 1];
}

void clean_val_stack(ValStack *stack){
    stack->service = 5;
    stack->size = 0;
    stack->mas = realloc(stack->mas, sizeof (val_type) * stack->service);
}

SignStack *init_sign_stack(void){
    SignStack *stack = malloc(sizeof(SignStack));
    stack->size = 0;
    stack->service = 5;
    stack->mas = malloc(sizeof(char) * stack->service);
    return stack;
}

void delete_sign_stack(SignStack *stack){
    free(stack->mas);
    free(stack);
}

void push_sign(SignStack *stack, char sign){
    if (stack->size >= stack->service){
        stack->service += 50;
        stack->mas =
                realloc(stack->mas, sizeof (long double) * stack->service);
    }
    stack->mas[stack->size++] = sign;
}

int pop_sign(SignStack *stack){
    if (stack->size <= 0){
        return EMPTY_SIGN_STACK;
    }
    stack->size--;
    return 0;
}

char top_sign(SignStack *stack, int *status){
    if (stack->size <= 0){
        if (status != NULL){
            *status = EMPTY_SIGN_STACK;
        }
        return 0;
    }
    if (status != NULL){
        *status = 0;
    }
    return stack->mas[stack->size - 1];
}

void clean_sign_stack(SignStack *stack){
    stack->service = 5;
    stack->size = 0;
    stack->mas = realloc(stack->mas, sizeof (char) * stack->service);
}

void process_variable(Calculator *this){
    Calculator *obj = init_calculator();
    delete_var_store(obj->var_store);
    obj->var_store = this->var_store;
    while (1){
        reset_calculator(obj);
        printf("%s = ", this->token);
        get_line(obj);
        if (calculating(obj, DEFAULT) != 0){
            print_error_msg(obj);
            continue;
        }
        val_type val = obj->val_stack->mas[0];
        add_value(this->var_store, this->token, val);
        push_value(this->val_stack, val);
        break;
    }
    obj->var_store = NULL;
    delete_calculator(obj);
}

VariableStore* init_var_store(void){
    VariableStore *store = malloc(sizeof (VariableStore));
    store->service = 5;
    store->size = 0;
    store->mas = malloc(sizeof (Variable) * store->service);
    return store;
}

void delete_var_store(VariableStore *store){
    if (store == NULL){
        return;
    }
    free(store->mas);
    free(store);
}

void add_value(VariableStore *store, char *name, val_type value){
    Variable *check = get_var(store, name);
    if (check == NULL){
        if (store->size >= store->service){
            store->service += 20;
            store->mas = realloc(store->mas, sizeof (Variable) * store->service);
        }
        strcpy(store->mas[store->size].name, name);
        store->mas[store->size].value = value;
        store->size++;
    }else{
        check->value = value;
    }
}

Variable* get_var(VariableStore *store, char *name){
    for (long i = 0; i < store->size; i++){
        if (0 == strcmp(name, store->mas[i].name)){
            return &store->mas[i];
        }
    }
    return NULL;
}

int find_all_variables(Calculator *this){
    this->ptr = 0;
    while (1){
        get_tok(this);
        if (this->type == ERROR){
            return this->err;
        }else if (this->type == END){
            break;
        }else if (this->type == VARIABLE){
            add_value(this->var_store, this->token, 0);
        }
    }
    this->ptr = 0;
    return 0;
}

int input_all_variables(Calculator *this){
    VariableStore *store = this->var_store;
    Calculator *obj = init_calculator();
    for (long i = 0; i < store->size; i++){
        reset_calculator(obj);
        get_line(obj);
        val_type value;
        char *end;
#ifdef int_ari
        value = strtol(obj->str, &end, 10);
#endif
#ifdef float_ari
        value = strtod(obj->str, &end);
#endif
        if (errno == ERANGE){
            delete_calculator(obj);
            this->err = VALUE_TOK_ERR;
            return this->err;
        }
        while (isspace(*end) && *end != '\0'){
            end++;
        }
        if (*end != '\0'){
            strcpy(this->token, obj->str);
            delete_calculator(obj);
            this->err = VALUE_TOK_ERR;
            return this->err;
        }
        store->mas[i].value = value;
    }
    delete_calculator(obj);
    return 0;
}

int comp_variables(Variable *first, Variable *second){
    return strcmp(first->name, second->name);
}

void sort_var_store(VariableStore *store){
    qsort(store->mas, store->size, sizeof (Variable), (comp) comp_variables);
}