#include <stdio.h>
#include "calculator.h"

int main(int argc, char *argv[]){
    Calculator *calc = init_calculator();
    int code = contest_process(calc);
    delete_calculator(calc);
    return code;
}
