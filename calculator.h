#define int_ari int
#define val_type long
#define float_compare(x, y) (fabs((double) (x) - (double) (y)) < (double) 0.000000000001)
#define MAC_UN_PLUS ((char) 1)
#define MAC_UN_MINUS ((char) 2)

enum {  MAX_TOK_LENGTH = 100,
    MAX_VARIABLE_LENGTH = 6 };

typedef enum {
    PLUS,
    MINUS,
    UN_PLUS,
    UN_MINUS,
    DIVIDE,
    MULTIPLY,
    VALUE,
    END,
    VARIABLE,
    OPEN,
    CLOSE,
    ERROR
} Type;

typedef enum {
    VALUE_TOK_ERR = 1,
    VARIABLE_TOK_ERR,
    BIG_VARIABLE_ERR,
    BIG_TOK_ERR,
    EMPTY_VALUE_STACK,
    EMPTY_SIGN_STACK,
    NOT_CALCULABLE_SIGN,
    MISS_OPERATOR,
    MISS_OPERAND,
    EMPTY_LINE,
    OUT_RANGE,
    MISS_CLOSE_BRACKET,
    MISS_OPEN_BRACKET,
    NULL_DIVIDING,
    UNKNOWN_MODE,
    UNKNOWN_OPERATOR = 100,
    NOTHING
} ErrorType;

typedef enum {
    CHECK,
    DEFAULT
} CalculatingMode;

typedef struct {
    char name[MAX_VARIABLE_LENGTH + 1];
    val_type value;
} Variable;

typedef struct {
    char *mas;
    long size;
    long service;
} SignStack;

typedef struct {
    val_type *mas;
    long size;
    long service;
} ValStack;

typedef struct {
    Variable *mas;
    long size;
    long service;
} VariableStore;

typedef struct {
    char *str;
    long str_size;
    long service;
    long ptr;
    char token[MAX_TOK_LENGTH + 1];
    long tok_size;
    Type type;
    ErrorType err;
    FILE *err_file;
    ValStack *val_stack;
    SignStack *sign_stack;
    VariableStore *var_store;
} Calculator;

typedef int (*comp) (const void *, const void *);

Calculator* init_calculator(void);
void delete_calculator(Calculator *this);
void reset_calculator(Calculator *this);

int contest_process(Calculator *this);
void user_process(Calculator *this);
int calculating(Calculator *this, CalculatingMode mode);
int find_all_variables(Calculator *this);
void process_variable(Calculator *this);
int process_sign(Calculator *this, int cnt, CalculatingMode mode);
int get_level(int sym);
int calculate_top(Calculator *this, CalculatingMode mode);

void get_tok(Calculator *this);
void get_line(Calculator *this);
int check_start(Calculator *this, char *str);

long find_service_sym(char* dest, long start);
int check_service_sym(int sym);

int input_all_variables(Calculator *this);
void print_error_msg(Calculator *this);
void print_current_token(Calculator *this);

ValStack *init_val_stack(void);
void delete_val_stack(ValStack *stack);
void push_value(ValStack *stack, val_type value);
int pop_value(ValStack *stack);
val_type top_value(ValStack *stack, int *status);
void clean_val_stack(ValStack *stack);

SignStack *init_sign_stack(void);
void delete_sign_stack(SignStack *stack);
void push_sign(SignStack *stack, char sign);
int pop_sign(SignStack *stack);
char top_sign(SignStack *stack, int *status);
void clean_sign_stack(SignStack *stack);

VariableStore* init_var_store(void);
void delete_var_store(VariableStore *store);
void add_value(VariableStore *store, char *name, val_type value);
Variable* get_var(VariableStore *store, char *name);
int comp_variables(Variable *first, Variable *second);
void sort_var_store(VariableStore *store);
